'use strict'

const cheerio = require('cheerio');
const rp = require('request-promise');

function parser(pagesNum = 1) {
   return new Promise (function(resolve, reject) {
      const options = {
         uri: 'https://www.057.ua/rss',
         transform: function(body) {
            return cheerio.load(body, {
               normalizeWhitespace: true,
               xmlMode: true,
            });
         }
      };

      const pages = [];

      rp(options)
         .then(function($) {
            let items = $('item');
            if(items.length < pagesNum) {
               pagesNum = items.length;
            }

            items.slice(0,pagesNum).each(function(i, elm) {
               pages[pages.length] = {};
               pages[pages.length - 1].link = $(this).find('link').text();
            });

            const newsLinks = pages.map(page=>page.link);

            const linksPromises = newsLinks.map(function (link) {
               const options = {
                  uri: link,
                  transform: function(body) {
                     return cheerio.load(body, {
                        normalizeWhitespace: true,
                        xmlMode: false,
                     });
                  }
               }
               return rp(options);
            });
            return Promise.all(linksPromises);
         })
         .then(function(rawPages) {
            rawPages.forEach(function($, idx) {
               pages[idx].header = $('.text_header').text();
               pages[idx].image = $('.inner-photo-block .img-block img').attr('src');
               pages[idx].text = $('#content .static').html();
            });
            resolve(pages);
         })
         .catch(function(err) {
            console.log('ERROR:', err);
         });
   })
}

module.exports = parser;