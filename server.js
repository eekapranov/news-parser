'use strict'

const express = require('express');
const app = express();
const Handlebars = require('handlebars');

const parser = require('./parser.js');

app.get('/api/news', function(req, res) {
   parser(5).then(function(data) {
      let templateSrc = '<h1>News feed</h1>'
         +'<div class="container">{{#pages}}'
         +'<h2>{{header}}</h2><div><img src="{{image}}" alt="{{header}}" />'
         +'<div>{{{text}}}</div><p>Source link: <a href="{{link}}">Портал 057 - {{link}}</a></p></div>{{/pages}}</div>';
      const template = Handlebars.compile(templateSrc);

      let responseHtml = template({pages: data});

      console.log('send /api/news response');
      res.send(responseHtml);
   });
})

app.listen(3000, function () {
   console.log('Server listening http://localhost:3000');
});